# Customised EMACS
This project is inspired from one of the best open source projects out there  by [Yesudeep Mangalapilly](https://github.com/gorakhargosh/gemacs). Purpose of this project is to install  plugins in emacs corp network. Challenge is plugin install is only allowed for github and blocked for all network proxies.

# Installation
```
git clone https://tarunsai284@bitbucket.org/tarunsai284/xtea-encription.git
cd customised-emacs
./setup.sh
```
